﻿# -*- coding: utf-8 -*-
"""
Created on Fri Dec 30 10:45:47 2016

@author: hutia
"""

import zmq
import time
import random
import logging

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger_info = logging.getLogger('info')
logger_info.setLevel(logging.INFO)
sh_info = logging.StreamHandler()
sh_info.setLevel(logging.INFO)
sh_info.setFormatter(formatter)
fh_info = logging.FileHandler('{}_info.log'.format(time.strftime("%Y%m%d_%H%M%S")))
fh_info.setLevel(logging.INFO)
fh_info.setFormatter(formatter)
logger_info.addHandler(sh_info)
logger_info.addHandler(fh_info)

# init env
instrument_list = ['if2005']
position = [0]
batch_num = 25
order_limit = 5
interval = 120


logger_info.info('instrument_list = {}'.format(instrument_list))
logger_info.info('initial position = {}'.format(position))
logger_info.info('batch_num = {}'.format(batch_num))
logger_info.info('order_limit = {}'.format(order_limit))
logger_info.info('interval = {}'.format(interval))

# init zmq
context = zmq.Context()
print("Connecting to zmq server")
socket = context.socket(zmq.REQ)  
socket.connect ("tcp://localhost:5556")

print("press enter to start simulator")
input()  

for i in range(batch_num):
    for index, instrument in enumerate(instrument_list):
        # random
        lots = random.randint(-order_limit, order_limit)
        if lots > 0:
            msg = 'buy {} {} by best'.format(instrument, lots)
            position[index] += lots
        if lots < 0:
            msg = 'sell {} {} by best'.format(instrument, -lots)
            position[index] += lots
        if lots == 0:
            print("lots = 0")
            continue
        socket.send_string(msg)
        rtn_msg = socket.recv_string()
        logger_info.info(msg)
        logger_info.info(rtn_msg)
    time.sleep(interval)
	
	
logger_info.info("target position")
logger_info.info(instrument_list)
logger_info.info(position)