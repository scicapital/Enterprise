CREATE USER enterprise3@'%' IDENTIFIED BY 'enterprise3';

GRANT SELECT ON *.* TO enterprise3;
GRANT INSERT,UPDATE,DELETE ON enterprise3.* TO enterprise3;

FLUSH PRIVILEGES;
SHOW GRANTS FOR enterprise3;
