![](doc/assets/0_front.PNG)

# Enterprise3

## 项目背景 

目前市面上存在着较多程序化交易平台，但少有平台能符合以下需求

1. 能用python, c++, c#, java, matlab等编程语言写策略。
   - 以便用自己熟悉的语言写策略
   - 以便调用第三方编写的库，如**scikit-learn**、**TensorFlow**等统计模型以及深度学习库
2. 能长期稳定运行
3. 能离线运行
   - 不与外部服务器通讯，防止策略泄露
   - 以便部署在期货公司内网中，使交易尽可能的快
4. 能定制算法交易
   - 以便减少大额交易对市场的冲击
   - 以便实现高级的套利算法
5. 能落地TICK数据
   - 以便盘后生成历史数据
   - 通过自己录数据，降低整体业务成本
6. 能自定义BAR
   - 以便避开拥挤交易
   - 用N个时间点把时间线切成N-1段，可切出任意时间长度的BAR，不局限于等周期BAR

我们自己CTA交易业务有以上需求，但又无法找到合适的商用平台。为了开展量化交易业务，我们自己开发了全套程序。


## 整体架构

量化交易系统分为 **预测** 和 **执行** 两个部分。

执行部分由交易管理终端 Trading Terminal-代号***Enterprise*** 组成。

预测部分由策略管理终端 Strategy Terminal-代号***Discovery*** 和 策略框架 Strategy Framework-代号***pyDiscovery*** 组成。

系统架构如下： 

![](doc/assets/1_topography.PNG)

## 交易执行终端 - Trading Terminal - Enterprise

Trading Terminal 首要目标是为**程序化交易员/数据科学家/量化投资研究员**提供一个通用的、不限制策略语言的交易执行终端，策略可以用通用计算机语言（策略支持python, c++, c#, java, matlab ...）编写；第二目标是为**一般投资者**提供一个算法交易和套利交易的终端，以便降低交易难度。

Trading Terminal 当前版本代号为Enterprise(以下简称Enterprise)，全部功能免费使用，对增值服务进行收费。增值服务包括：定制算法交易和定制软件功能。

安全、稳定优先，兼顾速度

基于安全的理念下，程序做了如下处理  

- 出现全局级别异常
  - 停止程序 
  - 交易前置机断线事件会被视为异常，程序会停止工作，由于不能确保交易数据的完整性，不会尝试自动连接；行情前置机断线事件，程序会尝试自动连接 
- 出现算法级别异常
  - 停止算法
  - 如果算法发现自己无法处理的情况，算法会停止工作，并发出消息通知用户
  - 未知状态的订单会被视为异常，该订单的算法会停止工作
- 出现订单级别的异常
  - 将订单标记为全部成交，避免反复下单和反复撤单
  - 下单失败的订单会被视为全部成交
  - 撤单失败的订单会被视为全部成交

基于稳定的理念下，程序做了如下处理  

- 核心程序和图形界面分离  
  - 核心程序将核心数据存到数据库
  - 图形界面程序读取数据库数据进行分析、监控和操作
- 执行与策略分离
  - 策略终端迭代快，执行终端迭代慢，尽量减少策略迭代对整个系统的影响
  - 一个执行终端可以对接多个策略终端，防止一个策略终端异常影响其他策略执行
- 不同期货账户分离
  - 一个执行器对应一个期货账户
  - 多账户可以通过开多个执行器实现，防止一个执行器异常影响到其他执行器

### 核心功能  

- 交易指令支持程序和手工输入
    - 通过网络发送消息
    - 手工在终端输入交易指令
- 算法交易  
    - 通过算法将大单拆成小单，减少对市场的冲击
- 套利交易
    - 针对不同流动性场景设计了多种执行方法
- 自动开平仓  
    - 优先平仓  
    - 支持反向开仓代替平今  
- 风险控制  
    - 品种黑名单管理
    - 指令风控
        - 合约名字校验  
        - 黑名单校验  
        - 手数校验
        - ~~金额校验~~
        - 价格校验
    - 事前风控  
        - ~~撤单次数校验~~
- 标准化合约
    - 统一为品种+4位数字交割年月形式，无需记忆郑商所3位数字交割年月
    - 无须区分大小写

### 概念

#### 模块 

1. 风险管理系统 - risk management system
1. 负责对指令进行风险控制校验
1. 执行管理系统 - execution management system
   1. 负责管理算法交易
1. 订单管理系统 - order management system
   1. 负责对持仓进行管理

#### 订单

1. 用户单 - User Order

用户下达的交易指令

2. 逻辑单 - Logical Order

根据算法交易拆成N张逻辑单

3. 实体单 - Physical Order

每张逻辑单根据交易所 开仓/平仓/平今仓 拆成N张最终实体单

#### 释义

1. 对手价

<div STYLE="page-break-after:always;"></div>

### 安装 

#### 编译环境 

| 项目       | 推荐                         |
| ---------- | ---------------------------- |
| 操作系统   | Windows Server 2016          |
| 开发工具   | Microsoft Visual Studio 2017 |
| 数据库     | mysql-5.7                    |
| 消息中间件 | zeromq-4.2.2                 |
| 日志库     | glog-0.4.0                   |


#### 运行环境

| 项目     | 推荐                                            |
| -------- | ----------------------------------------------- |
| 操作系统 | Windows Server 2016                             |
| 运行时   | Microsoft Visual C++ 2017 Redistributable (x64) |
| 数据库   | mysql 5.7                                       |

#### 安装步骤
1. 安装 Microsoft Visual C++ 2017 Redistributable (x64)
2. 安装 Mysql 5.7
3. 创建 数据库表结构
4. 创建 数据库用户

#### 建议
***建议严格按照运行环境表格中的环境来配置系统，确保不出现兼容性问题***

<div STYLE="page-break-after:always;"></div>

### 指令
### 撤单

#### 语法及用例
> DEL ***order_id***

> DEL 1


<div STYLE="page-break-after:always;"></div>

#### 限价指令  

#### 语法及用例  
> BUY/SELL ***INSTRUMENT_ID*** ***LOTS*** AT ***PRICE***    
> ~~BUY/SELL ***INSTRUMENT_ID*** ***LOTS*** BY LIMIT AT ***PRICE***~~  

> BUY IF1501 1 AT 3608.2  
> ~~BUY IF1501 1 BY LIMIT AT 3608.2~~  

#### 参数说明
| 参数类型 | 参数名称 | 数值类型 | 说明             |
| ------- | ------- | ---- | -------------- |
| 指令参数   | AT       | double | 限价指令中的价格 |

#### 应用场景
> 适用于不是必须要执行的交易，只有价格满足条件了才执行  

#### 算法行为
- 以用户指定的价格下单到市场，不做任何其他操作，直到单子成交或用户停止算法
#### 注意事项
- 当指令价格超出涨跌停范围时，会被风控系统拒绝  
- 当下单失败时，下单失败的单子会被视为全部成交处理，避免反复下单
- 停止算法时会撤销所有在交易所队列中的订单，但撤销所有订单时的那一刻有订单已发送但未在交易所队列中，那这个订单会被忽略，请手动撤单

<div STYLE="page-break-after:always;"></div>

### 市价指令  
#### 语法及用例  
> BUY/SELL ***INSTRUMENT_ID*** ***LOTS***  
> BUY/SELL ***INSTRUMENT_ID*** ***LOTS*** BY MARKET  

> BUY IF1501 1  
> BUY IF1501 1 BY MARKET

#### 参数说明
| 参数类型 | 参数名称 | 数值类型 | 说明             |
| ------- | ------- | ---- | -------------- |
| 配置参数 | chunk_size | int | 当指令数量大于此数量时，大于的部分会被递延交易 |
| 配置参数 | delay_time | int | 递延时间 |

#### 应用场景
> 适用于交易标的流动性较好且对交易执行时间较为急迫的场景，希望立刻执行掉交易

#### 算法行为
- 买单用涨停价下单，卖单用跌停价下单
- 当指令数量大于chunk_size时，大于chunk_size的部分会被递延交易
    - 先下chunk_size手
    - 等待delay_time秒时间
    - 再下chunk_size手，如此反复直至下单完成

#### 注意事项
无

<div STYLE="page-break-after:always;"></div>

### 最优价指令
#### 语法及用例
> BUY/SELL ***INSTRUMENT_ID*** ***LOTS*** BY BEST  

> BUY IF1501 1 BY BEST

#### 参数说明
| 参数类型 | 参数名称 | 数值类型 | 说明             |
| ------- | ------- | ---- | -------------- |
| 配置参数 | chunk_size | int | 当指令数量大于此数量时，大于的部分会被递延交易 |
| 配置参数 | delay_time | int | 递延时间 |
| 配置参数 | wait_time | int | 用最优价下单的时间 |
| 配置参数 | cancel_time | int | 撤单的间隔 |

#### 应用场景
> 适用于对交易执行不是非常急迫，希望通过挂单拿到更好价格的场景，当一定时间拿不到订单后，尝试去主动吃单

#### 算法行为
- 先尝试以最优价去拿单子，当超过一定时间拿不到单子后，尝试以对手价去拿
- 在时间计数器(wait_time_count)没有超过wait_time以前，会以最优价挂单
- 每隔cancel_time的时间判断一次当前挂单是否为最优价  
    - 如果是，不做任何事情  
    - 如果不是，撤单重新挂最优价
- 当wait_time_count超过wait_time以后，会以对手价下单
- 每隔cancel_time的时间判断一次当前挂单是否为对手价  
    - 如果是，不做任何事情  
    - 如果不是，撤单重新挂对手价
- 当指令数量大于chunk_size时，大于chunk_size的部分会被递延交易
    - 先下chunk_size手
    - 等待delay_time秒时间
    - 再下chunk_size手，如此反复直至下单完成

#### 注意事项
- 每当收到指令时会重置当前wait_time_count为0

<div STYLE="page-break-after:always;"></div>

### 市价套利指令
#### 语法及用例
> BUY/SELL ***INSTRUMENT_ID1-INSTRUMENT_ID2*** ***LOTS***  
> BUY/SELL ***INSTRUMENT_ID1-INSTRUMENT_ID2:MULTIPLE1/MULTIPLE2*** ***LOTS***  
> BUY/SELL ***INSTRUMENT_ID1-INSTRUMENT_ID2*** ***LOTS*** BY MARKET  
> BUY/SELL ***INSTRUMENT_ID1-INSTRUMENT_ID2:MULTIPLE1/MULTIPLE2*** ***LOTS*** BY MARKET
>
> ~~BUY/SELL ***INSTRUMENT_ID1/INSTRUMENT_ID2*** ***LOTS***~~  
> ~~BUY/SELL ***INSTRUMENT_ID1/INSTRUMENT_ID2:MULTIPLE1/MULTIPLE2*** ***LOTS***~~  
> ~~BUY/SELL ***INSTRUMENT_ID1/INSTRUMENT_ID2*** ***LOTS*** BY MARKET~~  
> ~~BUY/SELL ***INSTRUMENT_ID1/INSTRUMENT_ID2:MULTIPLE1/MULTIPLE2*** ***LOTS*** BY MARKET~~

> BUY IF1501-IF1502 2
> 会买入2手IF1501，卖出2手IF1502  
> 
> BUY J1905-JM1905:1-2 5 
> 会买入5手J1905，卖出10手JM1905  

#### 参数说明

无

#### 应用场景

> 当标的行情非常大时，希望不计代价立即执行套利交易

#### 算法行为
- 以涨跌停价同时对两个合约同时进行下单
- 只有当一个指令完成后才会执行下一个指令
  - 如果当前指令没有完成时收到新指令，会将新指令存储到队列等待执行

#### 注意事项
无

<div STYLE="page-break-after:always;"></div>

### 限价套利指令
#### 语法及用例
> BUY/SELL ***INSTRUMENT_ID1-INSTRUMENT_ID2:MULTIPLE1/MULTIPLE2*** ***LOTS*** AT ***PRICE***  
> ~~BUY/SELL ***INSTRUMENT_ID1-INSTRUMENT_ID2:MULTIPLE1/MULTIPLE2*** ***LOTS*** BY LIMIT AT ***PRICE***~~  
> 
> BUY/SELL ***INSTRUMENT_ID1/INSTRUMENT_ID2:MULTIPLE1/MULTIPLE2*** ***LOTS*** AT ***PRICE***  
> ~~BUY/SELL ***INSTRUMENT_ID1/INSTRUMENT_ID2:MULTIPLE1/MULTIPLE2*** ***LOTS*** BY LIMIT AT ***PRICE***~~  

> BUY IF1501-IF1502 2 AT 15.0  
> 当以IF1501-IF1502价差<=15.0时，以同步的方式用涨跌停价下单 IF1501和IF1502  
>
> BUY J1905-JM1905:1-2 2 AT 15.0   
> 以同步的方式下单J1905和JM1905，合约比例为1比2  

#### 参数说明

| 参数类型 | 参数名称 | 数值类型 | 说明             |
| -------- | -------- | -------- | ---------------- |
| 指令参数 | AT       | double   | 限价指令中的价格 |

#### 应用场景
> 适用于标的流动性较好的场景

#### 算法行为

#### 注意事项

<div STYLE="page-break-after:always;"></div>

### 最优价套利指令 (规划中)
#### 语法及用例
> BUY/SELL ***INSTRUMENT_ID1-INSTRUMENT_ID2*** ***LOTS*** BY BEST PRIOR ***INSTRUMENT_ID***  
> BUY/SELL ***INSTRUMENT_ID1-INSTRUMENT_ID2:MULTIPLE1-MULTIPLE2*** ***LOTS*** BY BEST PRIOR ***INSTRUMENT_ID***  




<div STYLE="page-break-after:always;"></div>

### 异步限价套利指令 (beta)
#### 语法及用例
> BUY/SELL ***INSTRUMENT_ID1-INSTRUMENT_ID2*** ***LOTS*** BY ASYNC AT ***PRICE*** PRIOR ***INSTRUMENT_ID***  
> BUY/SELL ***INSTRUMENT_ID1-INSTRUMENT_ID2:MULTIPLE1-MULTIPLE2*** ***LOTS*** BY ASYNC AT ***PRICE*** PRIOR ***INSTRUMENT_ID***  

> BUY IF1501-IF1502 2 BY ASYNC AT 15.0 PRIOR IF1502  
> 以异步的方式套利，当IF1502盘口的对手价满足价差时，以对手价去下单，不成交就撤单，根据成交情况动态以IF1501进行对冲  
### 参数说明

| 参数类型 | 参数名称 | 数值类型 | 说明             |
| -------- | -------- | -------- | ---------------- |
| 指令参数 | AT       | double   | 限价指令中的价格 |
| 指令参数 | PRIOR    | string   | 优先成交的合约   |

#### 应用场景

> 适用于标的流动性较差的场景
>
> 当流动性较差时，当以市价或涨停价下单时，成交价格存在着很大的不确定性。异步限价套利指令提供了一个尝试以对手价下单的算法。如果对手价拿到了，则用另一个合约去对冲。如果没拿到，且价差不符合要求时，会撤单。

#### 算法行为

#### 注意事项

- 动态对冲过程中遵循四舍五入的原则，例如处理M1909-A1909:3-2 时，当A1909成交了1手时，M1909应该对冲1.5手，对冲时会按2手去下单。

<div STYLE="page-break-after:always;"></div>

### 做市套利指令 (beta)
#### 语法及用例
> BUY/SELL ***INSTRUMENT_ID1-INSTRUMENT_ID2*** ***LOTS*** BY MM AT ***PRICE*** PRIOR ***INSTRUMENT_ID***  
> BUY/SELL ***INSTRUMENT_ID1-INSTRUMENT_ID2:MULTIPLE1/MULTIPLE2*** ***LOTS*** BY MM AT ***PRICE*** PRIOR ***INSTRUMENT_ID***  
>
> BUY/SELL ***INSTRUMENT_ID1/INSTRUMENT_ID2*** ***LOTS*** BY MM AT ***PRICE*** PRIOR ***INSTRUMENT_ID***  
> BUY/SELL ***INSTRUMENT_ID1/INSTRUMENT_ID2:MULTIPLE1/MULTIPLE2*** ***LOTS*** BY MM AT ***PRICE*** PRIOR ***INSTRUMENT_ID***  
> 
> MM : Make Market

> BUY IF1501-IF1502 2 BY MM AT 15.0 PRIOR IF1502  
> 以做市的方式进行套利，假设IF1502流动性较差(甚至没有报价)，根据价差测算挂单位置，先挂单IF1502，根据挂单成交情况动态以IF1501进行对冲  
### 参数说明

| 参数类型 | 参数名称 | 数值类型 | 说明             |
| -------- | -------- | -------- | ---------------- |
| 指令参数 | AT       | double   | 限价指令中的价格 |
| 指令参数 | PRIOR    | string   | 优先成交的合约   |

#### 应用场景

> 适用于标的流动性极差的场景
>
> 流动性较差的品种挂单较少甚至没有报价，套利者根据流动性较好的合约加上价差计算出他能接受的价格，套利者按照这个价格挂单到市场，然后拿到多少手就用另一个流动性较好的合约去对冲

#### 算法行为

#### 注意事项

- 动态对冲过程中遵循四舍五入的原则，例如处理M1909-A1909:3-2 时，当A1909成交了1手时，M1909应该对冲1.5手，对冲时会按2手去下单。

